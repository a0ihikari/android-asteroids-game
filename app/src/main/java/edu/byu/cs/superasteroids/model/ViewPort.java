package edu.byu.cs.superasteroids.model;

import android.graphics.Rect;

/**
 * Created by Weverson on 2/26/2016.
 */
public class ViewPort extends ObjInSpace{
    Rect viewport;

    public void setBorder(Rect r) {
        viewport = new Rect(0,0,getWidth(),getHeight());
        viewport.offsetTo(r.centerX() - ((viewport.right - viewport.left)/2),
                r.centerY() -((viewport.bottom - viewport.top)/2));
        setPositionX(viewport.centerX());
        setPositionY(viewport.centerY());
    }

    public Rect getBorder() {
        return viewport;
    }
}
