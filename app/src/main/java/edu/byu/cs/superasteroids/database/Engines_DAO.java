package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.Cannon;
import edu.byu.cs.superasteroids.model.Engine;

/**
 * Data Access Object for the Engines table.
 */
public class Engines_DAO extends Game_DAO {
    public static final Engines_DAO SINGLETON = new Engines_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        String [] attPoint;
        for (int i = 0; i < objArr.length(); ++i) {
            values.put("baseSpeed", objArr.getJSONObject(i).getString("baseSpeed"));
            values.put("baseTurnRate", objArr.getJSONObject(i).getString("baseTurnRate"));
            attPoint = objArr.getJSONObject(i).getString("attachPoint").split(",");
            values.put("attachPoint_X", attPoint[0]);
            values.put("attachPoint_Y", attPoint[1]);
            values.put("image", objArr.getJSONObject(i).getString("image"));
            values.put("imageWidth", objArr.getJSONObject(i).getString("imageWidth"));
            values.put("imageHeight", objArr.getJSONObject(i).getString("imageHeight"));
            long id = getDb().insert("Engines", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     * Creates an array of engine objects based on the parameter received
     * and the query made to the database.
     */
    public Engine[] create()  {
        Cursor cursor = getDb().rawQuery("SELECT * FROM Engines", EMPTY_ARRAY_OF_STRINGS);
        Engine[] engines = new Engine[cursor.getCount()];
        int count = 0;
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Engine engineObj = new Engine();
                engineObj.setBaseSpeed(cursor.getInt(1));
                engineObj.setBaseTurnRate(cursor.getInt(2));
                engineObj.setAttachPoint_X(cursor.getInt(3));
                engineObj.setAttachPoint_Y(cursor.getInt(4));
                engineObj.setImage(cursor.getString(5));
                engineObj.setWidth(cursor.getInt(6));
                engineObj.setHeight(cursor.getInt(7));

                engines[count] = engineObj;
                count++;

                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return engines;
    }
}
