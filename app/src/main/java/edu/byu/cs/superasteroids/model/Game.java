package edu.byu.cs.superasteroids.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.byu.cs.superasteroids.database.DbOpenHelper;
import edu.byu.cs.superasteroids.database.Asteroids_DAO;
import edu.byu.cs.superasteroids.database.BackgroundObj_DAO;
import edu.byu.cs.superasteroids.database.Cannons_DAO;
import edu.byu.cs.superasteroids.database.Engines_DAO;
import edu.byu.cs.superasteroids.database.ExtraParts_DAO;
import edu.byu.cs.superasteroids.database.LevelAsteroids_DAO;
import edu.byu.cs.superasteroids.database.LevelBackgroundObj_DAO;
import edu.byu.cs.superasteroids.database.Levels_DAO;
import edu.byu.cs.superasteroids.database.MainBodies_DAO;
import edu.byu.cs.superasteroids.database.PowerCores_DAO;
import edu.byu.cs.superasteroids.database.Game_DAO;

/**
 * It is a class that will be passed through the activities with all the needed
 * methods to initialize the database and query it. It also will pass all the
 * needed model objects through the activities.
 */
public class Game {
    private SQLiteDatabase db;
    public static final Game SINGLETON = new Game();
    private Ship ship = new Ship();
    /**
     * Initializes the database.
     */
    public void initializer (Context context){
        DbOpenHelper dbOpenHelper = new DbOpenHelper(context);
        db = dbOpenHelper.getWritableDatabase();
        Game_DAO.SINGLETON.setDb(db);
        BackgroundObj_DAO.SINGLETON.setDb(db);
        Asteroids_DAO.SINGLETON.setDb(db);
        Levels_DAO.SINGLETON.setDb(db);
        LevelBackgroundObj_DAO.SINGLETON.setDb(db);
        LevelAsteroids_DAO.SINGLETON.setDb(db);
        MainBodies_DAO.SINGLETON.setDb(db);
        Cannons_DAO.SINGLETON.setDb(db);
        ExtraParts_DAO.SINGLETON.setDb(db);
        Engines_DAO.SINGLETON.setDb(db);
        PowerCores_DAO.SINGLETON.setDb(db);
    }

    /**
     * Imports the json file to the database.
     * @return a boolean to show if the import went through or not.
     */
    public boolean importer (JSONObject rootObject){
        JSONArray objArr = null;
        boolean result = false;
        try {
            rootObject = rootObject.getJSONObject("asteroidsGame");
            objArr = rootObject.getJSONArray("objects");
            BackgroundObj_DAO.SINGLETON.clearDB("Background_Obj");
            BackgroundObj_DAO.SINGLETON.add(objArr);
            objArr = rootObject.getJSONArray("asteroids");
            Asteroids_DAO.SINGLETON.clearDB("Asteroids");
            Asteroids_DAO.SINGLETON.add(objArr);
            objArr = rootObject.getJSONArray("levels");
            Levels_DAO.SINGLETON.clearDB("Levels");
            Levels_DAO.SINGLETON.add(objArr);
            objArr = rootObject.getJSONArray("mainBodies");
            MainBodies_DAO.SINGLETON.clearDB("Main_Bodies");
            MainBodies_DAO.SINGLETON.add(objArr);
            objArr = rootObject.getJSONArray("cannons");
            Cannons_DAO.SINGLETON.clearDB("Cannons");
            Cannons_DAO.SINGLETON.add(objArr);
            objArr = rootObject.getJSONArray("extraParts");
            ExtraParts_DAO.SINGLETON.clearDB("Extra_Parts");
            ExtraParts_DAO.SINGLETON.add(objArr);
            objArr = rootObject.getJSONArray("engines");
            Engines_DAO.SINGLETON.clearDB("Engines");
            Engines_DAO.SINGLETON.add(objArr);
            objArr = rootObject.getJSONArray("powerCores");
            PowerCores_DAO.SINGLETON.clearDB("Power_Cores");
            PowerCores_DAO.SINGLETON.add(objArr);

            result = true;

        } catch (JSONException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    /**
     * It creates a level based on the parameter passed.
     * @param lvlNum is the level number that the game needs to create.
     * @return a level object with all the level information needed.
     */
    public Level createLevel(int lvlNum){
        return Levels_DAO.SINGLETON.create(lvlNum);
    }

    public Ship createShip(int index, int id){
        ship.setFinalParts(index,id);
        return ship;
    }

    public Ship getShip() {
        return ship;
    }
}
