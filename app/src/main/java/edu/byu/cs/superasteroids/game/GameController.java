package edu.byu.cs.superasteroids.game;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import edu.byu.cs.superasteroids.base.GameView;
import edu.byu.cs.superasteroids.base.IGameDelegate;
import edu.byu.cs.superasteroids.content.ContentManager;
import edu.byu.cs.superasteroids.drawing.DrawingHelper;
import edu.byu.cs.superasteroids.model.Cannon;
import edu.byu.cs.superasteroids.model.Engine;
import edu.byu.cs.superasteroids.model.ExtraParts;
import edu.byu.cs.superasteroids.model.Game;
import edu.byu.cs.superasteroids.model.Level;
import edu.byu.cs.superasteroids.model.MainBody;
import edu.byu.cs.superasteroids.model.Ship;
import edu.byu.cs.superasteroids.model.ViewPort;

/**
 * Created by Weverson on 2/25/2016.
 */
public class GameController implements IGameDelegate {
    private ContentManager content;
    private Ship ship;
    private Level level;
    private int lvlNum = 1;
    private ViewPort viewPort;
    private List<Integer> asteroids;
    private List<Integer> statcObjs;

    @Override
    public void update(double elapsedTime) {
        if (InputManager.movePoint != null){
            ship.updatePosition((int)InputManager.movePoint.x, (int)InputManager.movePoint.y);
        }
    }

    private void loadShip(){
        ship = Game.SINGLETON.getShip();
        content.loadImage(ship.getFinalShip()[0].getImage());
        content.loadImage(ship.getFinalShip()[1].getImage());
        content.loadImage(ship.getFinalShip()[2].getImage());
        content.loadImage(ship.getFinalShip()[3].getImage());
        content.loadImage(ship.getFinalShip()[4].getImage());
        ship.setBorder(level.getWorld());
        ship.setPositionX(level.getWorld().centerX());
        ship.setPositionY(level.getWorld().centerY());
    }

    private void loadLevel(){
        int id;
        asteroids = new ArrayList<>();
        statcObjs = new ArrayList<>();
        level = Game.SINGLETON.createLevel(lvlNum);
        level.setWorld();
        for (int i = 0; i < level.getAsteroids().length; i++){
            id =content.loadImage(level.getAsteroids()[i].getImage());
            asteroids.add(id);
        }
        for (int i = 0; i < level.getStaticObjs().length; i++) {
            id = content.loadImage(level.getStaticObjs()[i].getImage());
            statcObjs.add(id);
        }
    }

    private void loadViewPort(){
        viewPort = new ViewPort();
        viewPort.setPositionX(ship.getPositionX());
        viewPort.setPositionY(ship.getPositionY());
    }

    @Override
    public void loadContent(ContentManager _content) {
        content = _content;
        loadLevel();
        loadShip();
        loadViewPort();
    }

    @Override
    public void unloadContent(ContentManager _content) {

    }

    private void drawShip(){
        if (ship.getBorder().intersect(viewPort.getBorder())){
            int x = DrawingHelper.getGameViewWidth()/2;
            int y = DrawingHelper.getGameViewHeight()/2;
            DrawingHelper.drawImage(ship.getFinalParts()[0], x, y, 0, 0.5f, 0.5f, 255);

            x = (DrawingHelper.getGameViewWidth()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getCannonAttach_X()) - (ship.getFinalShip()[0].getWidth()/2)) +
                            ((ship.getFinalShip()[1].getWidth()/2) - (((Cannon)ship.getFinalShip()[1]).getAttachPoint_X())))/2);
            y = (DrawingHelper.getGameViewHeight()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getCannonAttach_Y()) - (ship.getFinalShip()[0].getHeight()/2)) +
                            ((ship.getFinalShip()[1].getHeight()/2) - (((Cannon)ship.getFinalShip()[1]).getAttachPoint_Y())))/2);
            DrawingHelper.drawImage(ship.getFinalParts()[1], x, y, 0, 0.5f, 0.5f, 255);

            x = (DrawingHelper.getGameViewWidth()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getEngineAttach_X()) - (ship.getFinalShip()[0].getWidth()/2)) +
                            ((ship.getFinalShip()[2].getWidth()/2) - (((Engine)ship.getFinalShip()[2]).getAttachPoint_X())))/2);
            y = (DrawingHelper.getGameViewHeight()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getEngineAttach_Y()) - (ship.getFinalShip()[0].getHeight()/2)) +
                            ((ship.getFinalShip()[2].getHeight()/2) - (((Engine)ship.getFinalShip()[2]).getAttachPoint_Y())))/2);
            DrawingHelper.drawImage(ship.getFinalParts()[2], x, y, 0, 0.5f, 0.5f, 255);

            x = (DrawingHelper.getGameViewWidth()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getExtraAttach_X()) - (ship.getFinalShip()[0].getWidth()/2)) +
                            ((ship.getFinalShip()[3].getWidth()/2) - (((ExtraParts)ship.getFinalShip()[3]).getAttachPoint_X())))/2);
            y = (DrawingHelper.getGameViewHeight()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getExtraAttach_Y()) - (ship.getFinalShip()[0].getHeight()/2)) +
                            ((ship.getFinalShip()[3].getHeight()/2) - (((ExtraParts)ship.getFinalShip()[3]).getAttachPoint_Y())))/2);
            DrawingHelper.drawImage(ship.getFinalParts()[3], x, y, 0, 0.5f, 0.5f, 255);

            x = (DrawingHelper.getGameViewWidth()/2);
            y = (DrawingHelper.getGameViewHeight()/2);
            DrawingHelper.drawImage(ship.getFinalParts()[4], x, y, 0, 0.5f, 0.5f, 255);
        }

    }

    private void drawAsteroids(){
        int x = (DrawingHelper.getGameViewWidth()/2) + (1500 - viewPort.getBorder().centerX());
        int y = (DrawingHelper.getGameViewHeight()/2) + (1500 - viewPort.getBorder().centerY());
        for(int i = 0; i < statcObjs.size(); i++){
            DrawingHelper.drawImage(statcObjs.get(i), x, y, 0, (float)level.getStaticObjs()[i].getScale(),
                    (float)level.getStaticObjs()[i].getScale(), 255);
        }
        for(int i = 0; i < asteroids.size(); i++){
            DrawingHelper.drawImage(asteroids.get(i), x, y, 0, (float)level.getAsteroids()[i].getScale(),
                    (float)level.getAsteroids()[i].getScale(), 255);
        }

    }

    @Override
    public void draw() {
        viewPort.setWidth(DrawingHelper.getGameViewWidth());
        viewPort.setHeight(DrawingHelper.getGameViewHeight());
        viewPort.setBorder(ship.getBorder());
        drawAsteroids();
        drawShip();

    }
}
