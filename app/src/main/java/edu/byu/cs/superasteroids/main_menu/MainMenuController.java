package edu.byu.cs.superasteroids.main_menu;

import edu.byu.cs.superasteroids.base.IView;
import edu.byu.cs.superasteroids.content.ContentManager;
import edu.byu.cs.superasteroids.model.Game;
import edu.byu.cs.superasteroids.model.Ship;

/**
 * Created by Weverson on 2/24/2016.
 */
public class MainMenuController implements IMainMenuController {
    IMainMenuView view;
    public MainMenuController(IMainMenuView _view){
        view = _view;
    }

    public void randomShip (){
        Ship ship;
        int random;
        int id;
        ship = Game.SINGLETON.getShip();
        ship.setParts();
        random = (int)(Math.random() * (ship.getMainBodies().length));
        id = ContentManager.getInstance().loadImage(ship.getMainBodies()[random].getImage());
        ship.setFinalParts(0, id);
        ship.getFinalShip()[0] = ship.getMainBodies()[random];

        random = (int)(Math.random() * (ship.getCannons().length));
        id = ContentManager.getInstance().loadImage(ship.getCannons()[random].getImage());
        ship.setFinalParts(1, id);
        ship.getFinalShip()[1] = ship.getCannons()[random];

        random = (int)(Math.random() * (ship.getEngines().length));
        id = ContentManager.getInstance().loadImage(ship.getEngines()[random].getImage());
        ship.setFinalParts(2, id);
        ship.getFinalShip()[2] = ship.getEngines()[random];

        random = (int)(Math.random() * (ship.getExtraParts().length));
        id = ContentManager.getInstance().loadImage(ship.getExtraParts()[random].getImage());
        ship.setFinalParts(3, id);
        ship.getFinalShip()[3] = ship.getExtraParts()[random];

        random = (int)(Math.random() * (ship.getPowerCores().length));
        id = ContentManager.getInstance().loadImage(ship.getPowerCores()[random].getImage());
        ship.setFinalParts(4, id);
        ship.getFinalShip()[4] = ship.getPowerCores()[random];
    }

    @Override
    public void onQuickPlayPressed() {
        randomShip();
        view.startGame();
    }

    @Override
    public IView getView() {
        return null;
    }

    @Override
    public void setView(IView view) {

    }
}
