package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.Level;
import edu.byu.cs.superasteroids.model.Asteroid;
import edu.byu.cs.superasteroids.model.StaticObj;

/**
 * Data Access Object for the Levels table.
 */
public class Levels_DAO extends Game_DAO {
    public static final Levels_DAO SINGLETON = new Levels_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        JSONArray objArr2;
        LevelBackgroundObj_DAO.SINGLETON.clearDB("Level_Background_Obj");
        LevelAsteroids_DAO.SINGLETON.clearDB("Level_Asteroids");
        for (int i = 0; i < objArr.length(); ++i) {
            values.put("number", objArr.getJSONObject(i).getString("number"));
            values.put("title", objArr.getJSONObject(i).getString("title"));
            values.put("hint", objArr.getJSONObject(i).getString("hint"));
            values.put("width", objArr.getJSONObject(i).getString("width"));
            values.put("height", objArr.getJSONObject(i).getString("height"));
            values.put("music", objArr.getJSONObject(i).getString("music"));
            objArr2 = objArr.getJSONObject(i).getJSONArray("levelObjects");
            LevelBackgroundObj_DAO.SINGLETON.add(objArr2, objArr.getJSONObject(i).getString("number"));
            objArr2 = objArr.getJSONObject(i).getJSONArray("levelAsteroids");
            LevelAsteroids_DAO.SINGLETON.add(objArr2, objArr.getJSONObject(i).getString("number"));
            long id = getDb().insert("Levels", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     * Creates an array of asteroid objects based on the parameter received
     * and the query made to the database.
     */
    private Asteroid[] createAsteroids(int lvlNum){
        Cursor cursor = getDb().rawQuery(
                "SELECT * FROM Level_Asteroids WHERE level_number = " + lvlNum,
                EMPTY_ARRAY_OF_STRINGS);
        int number;
        int id;
        int count = 0;
        Asteroid[] asteroids = new Asteroid[cursor.getCount()];
        try {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                number = cursor.getInt(1);
                id = cursor.getInt(2);
                asteroids[count] = Asteroids_DAO.SINGLETON.create(id);
                asteroids[count].setNumber(number);

                cursor.moveToNext();
                count++;
            }
        } finally {
            cursor.close();
        }
        return asteroids;
    }

    /**
     * Creates an array of background objects based on the parameter received
     * and the query made to the database.
     */
    private StaticObj[] createStaticObjs(int lvlNum){
        Cursor cursor = getDb().rawQuery(
                "SELECT * FROM Level_Background_Obj WHERE level_number = " + lvlNum,
                EMPTY_ARRAY_OF_STRINGS);
        int positionX;
        int positionY;
        int id;
        double scale;
        int count = 0;
        StaticObj[] staticObjs = new StaticObj[cursor.getCount()];
        try {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                positionX = cursor.getInt(1);
                positionY = cursor.getInt(2);
                id = cursor.getInt(3);
                scale = cursor.getDouble(4);
                staticObjs[count] = BackgroundObj_DAO.SINGLETON.create(id);
                staticObjs[count].setPositionX(positionX);
                staticObjs[count].setPositionY(positionY);
                staticObjs[count].setScale(scale);

                cursor.moveToNext();
                count++;
            }
        } finally {
            cursor.close();
        }
        return staticObjs;
    }

    /**
     * Creates a level object based on the parameter received
     * and the query made to the database.
     */
    public Level create(int lvlNum)  {
        Level lvlObj = new Level();
        Cursor cursor = getDb().rawQuery(
                "SELECT * FROM Levels WHERE number = " + lvlNum,
                EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                lvlObj.setNumber(cursor.getInt(1));
                lvlObj.setTitle(cursor.getString(2));
                lvlObj.setWidth(cursor.getInt(3));
                lvlObj.setHeight(cursor.getInt(4));
                lvlObj.setMusic(cursor.getString(5));
                lvlObj.setHint(cursor.getString(6));

                cursor.moveToNext();
            }
            lvlObj.setStaticObjs(createStaticObjs(lvlNum));
            lvlObj.setAsteroids(createAsteroids(lvlNum));
        } finally {
            cursor.close();
        }
        return lvlObj;
    }
}
