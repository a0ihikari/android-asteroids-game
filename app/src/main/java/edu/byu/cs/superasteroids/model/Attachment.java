package edu.byu.cs.superasteroids.model;

/**
 * It is a super class for all the ship attachment objects on the game.
 * @see MovingObj
 */
public class Attachment extends MovingObj {
    private int attachPoint_X;
    private int attachPoint_Y;

    public int getAttachPoint_X() {
        return attachPoint_X;
    }

    public void setAttachPoint_X(int attachPoint_X) {
        this.attachPoint_X = attachPoint_X;
    }

    public int getAttachPoint_Y() {
        return attachPoint_Y;
    }

    public void setAttachPoint_Y(int attachPoint_Y) {
        this.attachPoint_Y = attachPoint_Y;
    }
}
