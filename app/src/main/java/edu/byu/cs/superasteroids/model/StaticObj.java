package edu.byu.cs.superasteroids.model;

/**
 * It is a class that creates an background object that will not move
 * on the game.
 * @see ObjInSpace
 */
public class StaticObj extends ObjInSpace {

}
