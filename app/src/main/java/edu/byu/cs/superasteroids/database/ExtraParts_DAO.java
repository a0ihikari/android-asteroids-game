package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.Engine;
import edu.byu.cs.superasteroids.model.ExtraParts;

/**
 * Data Access Object for the Extra_Parts table.
 */
public class ExtraParts_DAO extends Game_DAO {
    public static final ExtraParts_DAO SINGLETON = new ExtraParts_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        String [] attPoint;
        for (int i = 0; i < objArr.length(); ++i) {
            attPoint = objArr.getJSONObject(i).getString("attachPoint").split(",");
            values.put("attachPoint_X", attPoint[0]);
            values.put("attachPoint_Y", attPoint[1]);
            values.put("image", objArr.getJSONObject(i).getString("image"));
            values.put("imageWidth", objArr.getJSONObject(i).getString("imageWidth"));
            values.put("imageHeight", objArr.getJSONObject(i).getString("imageHeight"));
            long id = getDb().insert("Extra_Parts", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     * Creates an array of extra parts objects based on the parameter received
     * and the query made to the database.
     */
    public ExtraParts[] create()  {
        Cursor cursor = getDb().rawQuery("SELECT * FROM Extra_Parts", EMPTY_ARRAY_OF_STRINGS);
        ExtraParts[] extraParts = new ExtraParts[cursor.getCount()];
        int count = 0;
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ExtraParts extraPartsObj = new ExtraParts();
                extraPartsObj.setAttachPoint_X(cursor.getInt(1));
                extraPartsObj.setAttachPoint_Y(cursor.getInt(2));
                extraPartsObj.setImage(cursor.getString(3));
                extraPartsObj.setWidth(cursor.getInt(4));
                extraPartsObj.setHeight(cursor.getInt(5));

                extraParts[count] = extraPartsObj;
                count++;

                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return extraParts;
    }
}
