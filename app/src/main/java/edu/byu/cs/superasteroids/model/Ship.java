package edu.byu.cs.superasteroids.model;

import android.graphics.Rect;

import edu.byu.cs.superasteroids.database.Cannons_DAO;
import edu.byu.cs.superasteroids.database.Engines_DAO;
import edu.byu.cs.superasteroids.database.ExtraParts_DAO;
import edu.byu.cs.superasteroids.database.MainBodies_DAO;
import edu.byu.cs.superasteroids.database.PowerCores_DAO;
import edu.byu.cs.superasteroids.drawing.DrawingHelper;

/**
 * It is a class that creates a ship object to be placed on the game.
 * @see MovingObj
 */
public class Ship extends MovingObj {
    private Cannon[] cannons;
    private Engine[] engines;
    private ExtraParts[] extraParts;
    private MainBody[] mainBodies;
    private PowerCores[] powerCores;
    private int[] parts = new int[5];
    private MovingObj[] finalShip = new MovingObj[5];
    private Rect border;

    public Rect getBorder() {
        return border;
    }

    public void setBorder(Rect r) {

        border = new Rect(0,0,finalShip[0].getWidth(), finalShip[0].getHeight());
        border.union(new Rect(0,0,finalShip[1].getWidth(), finalShip[1].getHeight()));
        border.union(new Rect(0,0,finalShip[2].getWidth(), finalShip[2].getHeight()));
        border.union(new Rect(0,0,finalShip[3].getWidth(), finalShip[3].getHeight()));
        border.offsetTo(((r.right - r.left)/2) - ((border.right - border.left)/2),
                ((r.bottom - r.top)/2) - ((border.bottom - border.top)/2));
        setPositionX(border.centerX());
        setPositionY(border.centerY());
    }

    public void updatePosition(int x, int y){
        x = x - DrawingHelper.getGameViewWidth()/2;
        y = y - DrawingHelper.getGameViewHeight()/2;
        border.offsetTo(border.left + (x/10),
                border.top + (y/10));
    }

    public MovingObj[] getFinalShip() {
        return finalShip;
    }

    public Cannon[] getCannons() {
        return cannons;
    }

    public void setCannons(Cannon[] cannons) {
        this.cannons = cannons;
    }

    public Engine[] getEngines() {
        return engines;
    }

    public void setEngines(Engine[] engines) {
        this.engines = engines;
    }

    public ExtraParts[] getExtraParts() {
        return extraParts;
    }

    public void setExtraParts(ExtraParts[] extraParts) {
        this.extraParts = extraParts;
    }

    public MainBody[] getMainBodies() {
        return mainBodies;
    }

    public void setMainBodies(MainBody[] mainBodies) {
        this.mainBodies = mainBodies;
    }

    public PowerCores[] getPowerCores() {
        return powerCores;
    }

    public void setPowerCores(PowerCores[] powerCores) {
        this.powerCores = powerCores;
    }

    /**
     * It sets all the arrays that contains all the possible parts
     * of a ship.
     */
    public void setParts(){
        setCannons(Cannons_DAO.SINGLETON.create());
        setEngines(Engines_DAO.SINGLETON.create());
        setExtraParts(ExtraParts_DAO.SINGLETON.create());
        setMainBodies(MainBodies_DAO.SINGLETON.create());
        setPowerCores(PowerCores_DAO.SINGLETON.create());
    }

    public void setFinalParts(int index, int id){
        parts[index] = id;
    }

    public int[] getFinalParts() {
        return parts;
    }
}
