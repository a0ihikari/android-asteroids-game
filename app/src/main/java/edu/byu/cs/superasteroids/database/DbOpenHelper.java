package edu.byu.cs.superasteroids.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * This class creates the database for the game.
 */
public class DbOpenHelper extends SQLiteOpenHelper {
    /**
     *SQL code to create the Background_Obj table.
     */
    private String CREATE_BACKGROUND_OBJ_TABLE =
            "CREATE TABLE Background_Obj " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   objects TEXT NOT NULL UNIQUE" +
                    ")";

    /**
     *SQL code to create the Asteroids table.
     */
    private String CREATE_ASTEROIDS_TABLE =
            "CREATE TABLE Asteroids " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   name TEXT NOT NULL UNIQUE," +
                    "   image TEXT NOT NULL," +
                    "   imageWidth INTEGER NOT NULL," +
                    "   imageHeight INTEGER NOT NULL" +
                    ")";
    /**
     *SQL code to create the Levels table.
     */
    private String CREATE_LEVELS_TABLE =
            "CREATE TABLE Levels " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   number INTEGER NOT NULL UNIQUE," +
                    "   title TEXT NOT NULL UNIQUE," +
                    "   width INTEGER NOT NULL," +
                    "   height INTEGER NOT NULL," +
                    "   music TEXT NOT NULL," +
                    "   hint TEXT NOT NULL" +
                    ")";
    /**
     *SQL code to create the Level_Background_Obj table.
     */
    private String CREATE_LEVEL_BACKGROUD_OBJ_TABLE =
            "CREATE TABLE Level_Background_Obj " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   position_X INTEGER," +
                    "   position_Y INTEGER," +
                    "   objectId INTEGER," +
                    "   scale REAL," +
                    "   level_number INTEGER NOT NULL" +
                    ")";
    /**
     *SQL code to create the Level_Asteroids table.
     */
    private String CREATE_LEVEL_ASTEROIDS_TABLE =
            "CREATE TABLE Level_Asteroids " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   number INTEGER NOT NULL," +
                    "   asteroidId INTEGER NOT NULL," +
                    "   level_number INTEGER NOT NULL" +
                    ")";
    /**
     *SQL code to create the Main_Bodies table.
     */
    private String CREATE_MAIN_BODIES_TABLE =
            "CREATE TABLE Main_Bodies " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   cannonAttach_X INTEGER NOT NULL," +
                    "   cannonAttach_Y INTEGER NOT NULL," +
                    "   engineAttach_X INTEGER NOT NULL," +
                    "   engineAttach_Y INTEGER NOT NULL," +
                    "   extraAttach_X INTEGER NOT NULL," +
                    "   extraAttach_Y INTEGER NOT NULL," +
                    "   image TEXT NOT NULL," +
                    "   imageWidth INTEGER NOT NULL," +
                    "   imageHeight INTEGER NOT NULL" +
                    ")";
    /**
     *SQL code to create the Cannons table.
     */
    private String CREATE_CANNONS_TABLE =
            "CREATE TABLE Cannons " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   attachPoint_X INTEGER NOT NULL," +
                    "   attachPoint_Y INTEGER NOT NULL," +
                    "   emitPoint_X INTEGER NOT NULL," +
                    "   emitPoint_Y INTEGER NOT NULL," +
                    "   image TEXT NOT NULL," +
                    "   imageWidth INTEGER NOT NULL," +
                    "   imageHeight INTEGER NOT NULL," +
                    "   attackImage TEXT NOT NULL," +
                    "   attackImageWidth INTEGER NOT NULL," +
                    "   attackImageHeight INTEGER NOT NULL," +
                    "   attackSound TEXT NOT NULL," +
                    "   damage INTEGER NOT NULL" +
                    ")";
    /**
     *SQL code to create the Extra_Parts table.
     */
    private String CREATE_EXTRA_PARTS_TABLE =
            "CREATE TABLE Extra_Parts " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   attachPoint_X INTEGER NOT NULL," +
                    "   attachPoint_Y INTEGER NOT NULL," +
                    "   image TEXT NOT NULL," +
                    "   imageWidth INTEGER NOT NULL," +
                    "   imageHeight INTEGER NOT NULL" +
                    ")";
    /**
     *SQL code to create the Engines table.
     */
    private String CREATE_ENGINES_TABLE =
            "CREATE TABLE Engines " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   baseSpeed INTEGER NOT NULL," +
                    "   baseTurnRate INTEGER NOT NULL," +
                    "   attachPoint_X INTEGER NOT NULL," +
                    "   attachPoint_Y INTEGER NOT NULL," +
                    "   image TEXT NOT NULL," +
                    "   imageWidth INTEGER NOT NULL," +
                    "   imageHeight INTEGER" +
                    ")";
    /**
     *SQL code to create the Power_Cores table.
     */
    private String CREATE_POWER_CORES_TABLE =
            "CREATE TABLE Power_Cores " +
                    "(" +
                    "   PID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "   cannonBoost INTEGER NOT NULL," +
                    "   engineBoost INTEGER NOT NULL," +
                    "   image TEXT NOT NULL" +
                    ")";
    /**
    * Database name.
    */
    private static final String DB_NAME = "Super_Asteroids.sqlite";
    /**
     *Database version.
     */
    private static final int DB_VERSION = 1;

    public DbOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    /**
     *Create the tables.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BACKGROUND_OBJ_TABLE);
        db.execSQL(CREATE_ASTEROIDS_TABLE);
        db.execSQL(CREATE_LEVELS_TABLE);
        db.execSQL(CREATE_LEVEL_BACKGROUD_OBJ_TABLE);
        db.execSQL(CREATE_LEVEL_ASTEROIDS_TABLE);
        db.execSQL(CREATE_MAIN_BODIES_TABLE);
        db.execSQL(CREATE_CANNONS_TABLE);
        db.execSQL(CREATE_EXTRA_PARTS_TABLE);
        db.execSQL(CREATE_ENGINES_TABLE);
        db.execSQL(CREATE_POWER_CORES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        return;
    }

}
