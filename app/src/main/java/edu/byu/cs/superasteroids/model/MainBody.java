package edu.byu.cs.superasteroids.model;

/**
 * It is a class for all the main body objects on the game.
 * @see MovingObj
 */
public class MainBody extends MovingObj{
    private int cannonAttach_X;
    private int cannonAttach_Y;
    private int engineAttach_X;
    private int engineAttach_Y;
    private int extraAttach_X;
    private int extraAttach_Y;

    public int getCannonAttach_X() {
        return cannonAttach_X;
    }

    public void setCannonAttach_X(int cannonAttach_X) {
        this.cannonAttach_X = cannonAttach_X;
    }

    public int getCannonAttach_Y() {
        return cannonAttach_Y;
    }

    public void setCannonAttach_Y(int cannonAttach_Y) {
        this.cannonAttach_Y = cannonAttach_Y;
    }

    public int getEngineAttach_X() {
        return engineAttach_X;
    }

    public void setEngineAttach_X(int engineAttach_X) {
        this.engineAttach_X = engineAttach_X;
    }

    public int getEngineAttach_Y() {
        return engineAttach_Y;
    }

    public void setEngineAttach_Y(int engineAttach_Y) {
        this.engineAttach_Y = engineAttach_Y;
    }

    public int getExtraAttach_X() {
        return extraAttach_X;
    }

    public void setExtraAttach_X(int extraAttach_X) {
        this.extraAttach_X = extraAttach_X;
    }

    public int getExtraAttach_Y() {
        return extraAttach_Y;
    }

    public void setExtraAttach_Y(int extraAttach_Y) {
        this.extraAttach_Y = extraAttach_Y;
    }
}
