package edu.byu.cs.superasteroids.model;

/**
 * It is a super class for all the objects that moves in space
 * on the game.
 * @see ObjInSpace
 */
public class MovingObj extends ObjInSpace {
}
