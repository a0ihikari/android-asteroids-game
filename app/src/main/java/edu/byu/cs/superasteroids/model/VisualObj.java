package edu.byu.cs.superasteroids.model;

/**
 * It is a super class for all the visual objects that will be used
 * on the game.
 */
public class VisualObj {
    private String image;
    private int width;
    private int height;

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
