package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Data Access Object for the Level_Asteroids table.
 */
public class LevelAsteroids_DAO extends Game_DAO {
    public static final LevelAsteroids_DAO SINGLETON = new LevelAsteroids_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr, String number) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        for (int i = 0; i < objArr.length(); ++i) {
            values.put("number", objArr.getJSONObject(i).getString("number"));
            values.put("asteroidId", objArr.getJSONObject(i).getString("asteroidId"));
            values.put("level_number", number);
            long id = getDb().insert("Level_Asteroids", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }
}
