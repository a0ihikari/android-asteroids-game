package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.PowerCores;

/**
 * Data Access Object for the Power_Cores table.
 */
public class PowerCores_DAO extends Game_DAO {
    public static final PowerCores_DAO SINGLETON = new PowerCores_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        for (int i = 0; i < objArr.length(); ++i) {
            values.put("cannonBoost", objArr.getJSONObject(i).getString("cannonBoost"));
            values.put("engineBoost", objArr.getJSONObject(i).getString("engineBoost"));
            values.put("image", objArr.getJSONObject(i).getString("image"));

            long id = getDb().insert("Power_Cores", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     * Creates an array of power cores objects based on the parameter received
     * and the query made to the database.
     */
    public PowerCores[] create()  {
        Cursor cursor = getDb().rawQuery("SELECT * FROM Power_Cores", EMPTY_ARRAY_OF_STRINGS);
        PowerCores[] powerCores = new PowerCores[cursor.getCount()];
        int count = 0;
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                PowerCores powerCoresObj = new PowerCores();
                powerCoresObj.setCannonBoost(cursor.getInt(1));
                powerCoresObj.setEngineBoost(cursor.getInt(2));
                powerCoresObj.setImage(cursor.getString(3));

                powerCores[count] = powerCoresObj;
                count++;

                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return powerCores;
    }
}
