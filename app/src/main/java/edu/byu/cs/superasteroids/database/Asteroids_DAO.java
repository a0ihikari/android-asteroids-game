package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import edu.byu.cs.superasteroids.model.Asteroid;
import edu.byu.cs.superasteroids.model.GrowingAst;
import edu.byu.cs.superasteroids.model.OcteroidAst;
import edu.byu.cs.superasteroids.model.RegularAst;

/**
 * Data Access Object for the Asteroids table.
 */
public class Asteroids_DAO extends Game_DAO {
    private String SELECT_REGULAR =
            "SELECT * FROM Asteroids WHERE name = \"regular\"";
    private String SELECT_GROWING =
            "SELECT * FROM Asteroids WHERE name = \"growing\"";
    private String SELECT_OCTEROID =
            "SELECT * FROM Asteroids WHERE name = \"octeroid\"";
    public static final Asteroids_DAO SINGLETON = new Asteroids_DAO();
    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        for (int i = 0; i < objArr.length(); ++i) {
            values.put("name", objArr.getJSONObject(i).getString("name"));
            values.put("image", objArr.getJSONObject(i).getString("image"));
            values.put("imageWidth", objArr.getJSONObject(i).getString("imageWidth"));
            values.put("imageHeight", objArr.getJSONObject(i).getString("imageHeight"));
            long id = getDb().insert("Asteroids", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }
    /**
     * Creates an asteroid object based on the parameter received
     * and the query made to the database.
     */
    public Asteroid create(int id)  {
        Asteroid ast = null;
        Cursor cursor = null;
        if (id == 1){
            ast = new RegularAst();
            cursor = getDb().rawQuery(SELECT_REGULAR, EMPTY_ARRAY_OF_STRINGS);
        } else if (id == 2){
            ast = new GrowingAst();
            cursor = getDb().rawQuery(SELECT_GROWING, EMPTY_ARRAY_OF_STRINGS);
        } else if (id == 3){
            ast = new OcteroidAst();
            cursor = getDb().rawQuery(SELECT_OCTEROID, EMPTY_ARRAY_OF_STRINGS);
        }
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ast.setName(cursor.getString(1));
                ast.setImage(cursor.getString(2));
                ast.setWidth(cursor.getInt(3));
                ast.setHeight(cursor.getInt(4));

                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return ast;
    }
}
