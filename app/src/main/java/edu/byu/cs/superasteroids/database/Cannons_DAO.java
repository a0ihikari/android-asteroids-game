package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.Cannon;

/**
 * Data Access Object for the Cannons table.
 */
public class Cannons_DAO extends Game_DAO {
    public static final Cannons_DAO SINGLETON = new Cannons_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        String [] attPoint;
        String [] emitPoint;
        for (int i = 0; i < objArr.length(); ++i) {
            attPoint = objArr.getJSONObject(i).getString("attachPoint").split(",");
            values.put("attachPoint_X", attPoint[0]);
            values.put("attachPoint_Y", attPoint[1]);
            emitPoint = objArr.getJSONObject(i).getString("emitPoint").split(",");
            values.put("emitPoint_X", emitPoint[0]);
            values.put("emitPoint_Y", emitPoint[1]);
            values.put("image", objArr.getJSONObject(i).getString("image"));
            values.put("imageWidth", objArr.getJSONObject(i).getString("imageWidth"));
            values.put("imageHeight", objArr.getJSONObject(i).getString("imageHeight"));
            values.put("attackImage", objArr.getJSONObject(i).getString("attackImage"));
            values.put("attackImageWidth", objArr.getJSONObject(i).getString("attackImageWidth"));
            values.put("attackImageHeight", objArr.getJSONObject(i).getString("attackImageHeight"));
            values.put("attackSound", objArr.getJSONObject(i).getString("attackSound"));
            values.put("damage", objArr.getJSONObject(i).getString("damage"));
            long id = getDb().insert("Cannons", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     * Creates an array of cannon objects based on the parameter received
     * and the query made to the database.
     */
    public Cannon[] create()  {
        Cursor cursor = getDb().rawQuery("SELECT * FROM Cannons", EMPTY_ARRAY_OF_STRINGS);
        Cannon[] cannons = new Cannon[cursor.getCount()];
        int count = 0;
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Cannon cannonObj = new Cannon();
                cannonObj.setAttachPoint_X(cursor.getInt(1));
                cannonObj.setAttachPoint_Y(cursor.getInt(2));
                cannonObj.setEmitPoint_X(cursor.getInt(3));
                cannonObj.setEmitPoint_Y(cursor.getInt(4));
                cannonObj.setImage(cursor.getString(5));
                cannonObj.setWidth(cursor.getInt(6));
                cannonObj.setHeight(cursor.getInt(7));
                cannonObj.setAttackImage(cursor.getString(8));
                cannonObj.setAttackImageWidth(cursor.getInt(9));
                cannonObj.setAttackImageHeight(cursor.getInt(10));
                cannonObj.setAttackSound(cursor.getString(11));
                cannonObj.setDamage(cursor.getInt(12));

                cannons[count] = cannonObj;
                count++;

                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return cannons;
    }
}
