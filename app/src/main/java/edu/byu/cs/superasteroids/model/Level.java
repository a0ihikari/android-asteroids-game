package edu.byu.cs.superasteroids.model;

import android.graphics.Rect;

/**
 * It is a class for the level objects on the game.
 */
public class Level {
    private int number;
    private String title;
    private int width;
    private int height;
    private String music;
    private String hint;
    private StaticObj[] staticObjs;
    private Asteroid[] asteroids;
    private Rect world;

    public void setWorld() {
        world = new Rect(0,0,width,height);
    }

    public Rect getWorld() {
        return world;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public void setStaticObjs(StaticObj[] staticObjs) {
        this.staticObjs = staticObjs;
    }

    public void setAsteroids(Asteroid[] asteroids) {
        this.asteroids = asteroids;
    }

    public int getNumber() {
        return number;
    }

    public String getTitle() {
        return title;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getMusic() {
        return music;
    }

    public String getHint() {
        return hint;
    }

    public StaticObj[] getStaticObjs() {
        return staticObjs;
    }

    public Asteroid[] getAsteroids() {
        return asteroids;
    }
}
