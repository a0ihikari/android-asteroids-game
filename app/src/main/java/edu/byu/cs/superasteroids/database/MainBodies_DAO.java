package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.MainBody;

/**
 * Data Access Object for the Main_Bodies table.
 */
public class MainBodies_DAO extends Game_DAO {
    public static final MainBodies_DAO SINGLETON = new MainBodies_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        String [] cannonAtt;
        String [] engineAtt;
        String [] extraAtt;
        for (int i = 0; i < objArr.length(); ++i) {
            cannonAtt = objArr.getJSONObject(i).getString("cannonAttach").split(",");
            values.put("cannonAttach_X", cannonAtt[0]);
            values.put("cannonAttach_Y", cannonAtt[1]);
            engineAtt = objArr.getJSONObject(i).getString("engineAttach").split(",");
            values.put("engineAttach_X", engineAtt[0]);
            values.put("engineAttach_Y", engineAtt[1]);
            extraAtt = objArr.getJSONObject(i).getString("extraAttach").split(",");
            values.put("extraAttach_X", extraAtt[0]);
            values.put("extraAttach_Y", extraAtt[1]);
            values.put("image", objArr.getJSONObject(i).getString("image"));
            values.put("imageWidth", objArr.getJSONObject(i).getString("imageWidth"));
            values.put("imageHeight", objArr.getJSONObject(i).getString("imageHeight"));
            long id = getDb().insert("Main_Bodies", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     * Creates an array of main body objects based on the parameter received
     * and the query made to the database.
     */
    public MainBody[] create()  {
        Cursor cursor = getDb().rawQuery("SELECT * FROM Main_Bodies", EMPTY_ARRAY_OF_STRINGS);
        MainBody[] mainBodies = new MainBody[cursor.getCount()];
        int count = 0;
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                MainBody mainBodieObj = new MainBody();
                mainBodieObj.setCannonAttach_X(cursor.getInt(1));
                mainBodieObj.setCannonAttach_Y(cursor.getInt(2));
                mainBodieObj.setEngineAttach_X(cursor.getInt(3));
                mainBodieObj.setEngineAttach_Y(cursor.getInt(4));
                mainBodieObj.setExtraAttach_X(cursor.getInt(5));
                mainBodieObj.setExtraAttach_Y(cursor.getInt(6));
                mainBodieObj.setImage(cursor.getString(7));
                mainBodieObj.setWidth(cursor.getInt(8));
                mainBodieObj.setHeight(cursor.getInt(9));

                mainBodies[count] = mainBodieObj;
                count++;

                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return mainBodies;
    }
}
