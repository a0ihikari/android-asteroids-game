package edu.byu.cs.superasteroids.ship_builder;

import java.util.ArrayList;
import java.util.List;

import edu.byu.cs.superasteroids.base.IGameDelegate;
import edu.byu.cs.superasteroids.base.IView;
import edu.byu.cs.superasteroids.content.ContentManager;
import edu.byu.cs.superasteroids.drawing.DrawingHelper;
import edu.byu.cs.superasteroids.model.Cannon;
import edu.byu.cs.superasteroids.model.Engine;
import edu.byu.cs.superasteroids.model.ExtraParts;
import edu.byu.cs.superasteroids.model.Game;
import edu.byu.cs.superasteroids.model.MainBody;
import edu.byu.cs.superasteroids.model.Ship;

/**
 * Created by Weverson on 2/20/2016.
 */
public class ShipBuildingController implements IShipBuildingController, IGameDelegate {
    private ShipBuildingActivity activity;
    private String currentView = "MAIN_BODY";
    private ContentManager content;
    private int mainBody;
    private int cannon;
    private int engine;
    private int extraPart;
    private int powerCore;
    private List<Integer> cannons;
    private List<Integer> engines;
    private List<Integer> extraParts;
    private List<Integer> mainBodies;
    private List<Integer> powerCores;
    private Ship ship = Game.SINGLETON.getShip();

    public ShipBuildingController(ShipBuildingActivity _activity) {
        activity = _activity;
    }

    @Override
    public void onViewLoaded(IShipBuildingView.PartSelectionView partView) {
        activity.setArrow(partView, IShipBuildingView.ViewDirection.UP,false,null);
        activity.setArrow(partView, IShipBuildingView.ViewDirection.DOWN,false,null);
    }

    @Override
    public void update(double elapsedTime) {

    }

    @Override
    public void loadContent(ContentManager _content) {
        content = _content;
        cannons = new ArrayList<>();
        engines = new ArrayList<>();
        extraParts = new ArrayList<>();
        mainBodies = new ArrayList<>();
        powerCores = new ArrayList<>();
        ship.setParts();
        for (int i = 0; i < ship.getCannons().length; i++) {
            content.loadImage(ship.getCannons()[i].getImage());
            cannons.add(content.getImageId(ship.getCannons()[i].getImage()));
        }
        for (int i = 0; i < ship.getEngines().length; i++) {
            content.loadImage(ship.getEngines()[i].getImage());
            engines.add(content.getImageId(ship.getEngines()[i].getImage()));
        }
        for (int i = 0; i < ship.getExtraParts().length; i++) {
            content.loadImage(ship.getExtraParts()[i].getImage());
            extraParts.add(content.getImageId(ship.getExtraParts()[i].getImage()));
        }
        for (int i = 0; i < ship.getMainBodies().length; i++) {
            content.loadImage(ship.getMainBodies()[i].getImage());
            mainBodies.add(content.getImageId(ship.getMainBodies()[i].getImage()));
        }
        for (int i = 0; i < ship.getPowerCores().length; i++) {
            content.loadImage(ship.getPowerCores()[i].getImage());
            powerCores.add(content.getImageId(ship.getPowerCores()[i].getImage()));
        }
        activity.setPartViewImageList(IShipBuildingView.PartSelectionView.CANNON,cannons);
        activity.setPartViewImageList(IShipBuildingView.PartSelectionView.ENGINE,engines);
        activity.setPartViewImageList(IShipBuildingView.PartSelectionView.EXTRA_PART,extraParts);
        activity.setPartViewImageList(IShipBuildingView.PartSelectionView.MAIN_BODY,mainBodies);
        activity.setPartViewImageList(IShipBuildingView.PartSelectionView.POWER_CORE,powerCores);
    }

    @Override
    public void unloadContent(ContentManager _content) {
        content = _content;
        for (int i = 0; i < ship.getMainBodies().length; i++) {
            if(content.getImageId(ship.getMainBodies()[i].getImage()) != ship.getFinalParts()[0]) {
                content.unloadImage(content.getImageId(ship.getMainBodies()[i].getImage()));
            }
        }
        for (int i = 0; i < ship.getCannons().length; i++) {
            if(content.getImageId(ship.getCannons()[i].getImage()) != ship.getFinalParts()[1]) {
                content.unloadImage(content.getImageId(ship.getCannons()[i].getImage()));
            }
        }
        for (int i = 0; i < ship.getEngines().length; i++) {
            if(content.getImageId(ship.getEngines()[i].getImage()) != ship.getFinalParts()[2]) {
                content.unloadImage(content.getImageId(ship.getEngines()[i].getImage()));
            }
        }
        for (int i = 0; i < ship.getExtraParts().length; i++) {
            if(content.getImageId(ship.getExtraParts()[i].getImage()) != ship.getFinalParts()[3]) {
                content.unloadImage(content.getImageId(ship.getExtraParts()[i].getImage()));
            }
        }
        for (int i = 0; i < ship.getPowerCores().length; i++) {
            if(content.getImageId(ship.getPowerCores()[i].getImage()) != ship.getFinalParts()[4]) {
                content.unloadImage(content.getImageId(ship.getPowerCores()[i].getImage()));
            }
        }
    }

    @Override
    public void draw() {
        int x = DrawingHelper.getGameViewWidth()/2;
        int y = DrawingHelper.getGameViewHeight()/2;
        if (mainBody == 1) {
            DrawingHelper.drawImage(ship.getFinalParts()[0], x, y, 0, 0.5f, 0.5f, 255);
        }
        if (cannon == 1) {
            x = (DrawingHelper.getGameViewWidth()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getCannonAttach_X()) - (ship.getFinalShip()[0].getWidth()/2)) +
                    ((ship.getFinalShip()[1].getWidth()/2) - (((Cannon)ship.getFinalShip()[1]).getAttachPoint_X())))/2);
            y = (DrawingHelper.getGameViewHeight()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getCannonAttach_Y()) - (ship.getFinalShip()[0].getHeight()/2)) +
                    ((ship.getFinalShip()[1].getHeight()/2) - (((Cannon)ship.getFinalShip()[1]).getAttachPoint_Y())))/2);
            DrawingHelper.drawImage(ship.getFinalParts()[1], x, y, 0, 0.5f, 0.5f, 255);
        }
        if (engine == 1){
            x = (DrawingHelper.getGameViewWidth()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getEngineAttach_X()) - (ship.getFinalShip()[0].getWidth()/2)) +
                    ((ship.getFinalShip()[2].getWidth()/2) - (((Engine)ship.getFinalShip()[2]).getAttachPoint_X())))/2);
            y = (DrawingHelper.getGameViewHeight()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getEngineAttach_Y()) - (ship.getFinalShip()[0].getHeight()/2)) +
                    ((ship.getFinalShip()[2].getHeight()/2) - (((Engine)ship.getFinalShip()[2]).getAttachPoint_Y())))/2);
            DrawingHelper.drawImage(ship.getFinalParts()[2], x, y, 0, 0.5f, 0.5f, 255);
        }
        if (extraPart == 1){
            x = (DrawingHelper.getGameViewWidth()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getExtraAttach_X()) - (ship.getFinalShip()[0].getWidth()/2)) +
                    ((ship.getFinalShip()[3].getWidth()/2) - (((ExtraParts)ship.getFinalShip()[3]).getAttachPoint_X())))/2);
            y = (DrawingHelper.getGameViewHeight()/2) +
                    ((((((MainBody)ship.getFinalShip()[0]).getExtraAttach_Y()) - (ship.getFinalShip()[0].getHeight()/2)) +
                    ((ship.getFinalShip()[3].getHeight()/2) - (((ExtraParts)ship.getFinalShip()[3]).getAttachPoint_Y())))/2);
            DrawingHelper.drawImage(ship.getFinalParts()[3], x, y, 0, 0.5f, 0.5f, 255);
        }
        if(powerCore == 1){
            x = (DrawingHelper.getGameViewWidth()/2);
            y = (DrawingHelper.getGameViewHeight()/2);
            DrawingHelper.drawImage(ship.getFinalParts()[4], x, y, 0, 0.5f, 0.5f, 255);
        }
    }

    @Override
    public void onSlideView(IShipBuildingView.ViewDirection direction) {
        if (currentView == "MAIN_BODY") {
            if (direction == IShipBuildingView.ViewDirection.LEFT) {
                currentView = "CANNON";
                activity.animateToView(IShipBuildingView.PartSelectionView.CANNON, IShipBuildingView.ViewDirection.RIGHT);
            } else {
                currentView = "POWER_CORE";
                activity.animateToView(IShipBuildingView.PartSelectionView.POWER_CORE, IShipBuildingView.ViewDirection.LEFT);
            }
        } else if (currentView == "CANNON"){
            if (direction == IShipBuildingView.ViewDirection.LEFT) {
                currentView = "ENGINE";
                activity.animateToView(IShipBuildingView.PartSelectionView.ENGINE, IShipBuildingView.ViewDirection.RIGHT);
            } else {
                currentView = "MAIN_BODY";
                activity.animateToView(IShipBuildingView.PartSelectionView.MAIN_BODY, IShipBuildingView.ViewDirection.LEFT);
            }
        } else if (currentView == "ENGINE"){
            if (direction == IShipBuildingView.ViewDirection.LEFT) {
                currentView = "EXTRA_PART";
                activity.animateToView(IShipBuildingView.PartSelectionView.EXTRA_PART, IShipBuildingView.ViewDirection.RIGHT);
            } else {
                currentView = "CANNON";
                activity.animateToView(IShipBuildingView.PartSelectionView.CANNON, IShipBuildingView.ViewDirection.LEFT);
            }
        } else if (currentView == "EXTRA_PART"){
            if (direction == IShipBuildingView.ViewDirection.LEFT) {
                currentView = "POWER_CORE";
                activity.animateToView(IShipBuildingView.PartSelectionView.POWER_CORE, IShipBuildingView.ViewDirection.RIGHT);
            } else {
                currentView = "ENGINE";
                activity.animateToView(IShipBuildingView.PartSelectionView.ENGINE, IShipBuildingView.ViewDirection.LEFT);
            }
        } else if (currentView == "POWER_CORE"){
            if (direction == IShipBuildingView.ViewDirection.LEFT) {
                currentView = "MAIN_BODY";
                activity.animateToView(IShipBuildingView.PartSelectionView.MAIN_BODY, IShipBuildingView.ViewDirection.RIGHT);
            } else {
                currentView = "EXTRA_PART";
                activity.animateToView(IShipBuildingView.PartSelectionView.EXTRA_PART, IShipBuildingView.ViewDirection.LEFT);
            }
        }
    }

    @Override
    public void onPartSelected(int index) {
        Ship ship;
        ship = Game.SINGLETON.getShip();
        if (currentView == "MAIN_BODY") {
            ship.setFinalParts(0,mainBodies.get(index));
            ship.getFinalShip()[0] = ship.getMainBodies()[index];
            mainBody = 1;
        } else if (currentView == "CANNON"){
            ship.setFinalParts(1,cannons.get(index));
            ship.getFinalShip()[1] = ship.getCannons()[index];
            cannon = 1;
        } else if (currentView == "ENGINE"){
            ship.setFinalParts(2,engines.get(index));
            ship.getFinalShip()[2] = ship.getEngines()[index];
            engine = 1;
        } else if (currentView == "EXTRA_PART"){
            ship.setFinalParts(3,extraParts.get(index));
            ship.getFinalShip()[3] = ship.getExtraParts()[index];
            extraPart = 1;
        } else if (currentView == "POWER_CORE"){
            ship.setFinalParts(4,powerCores.get(index));
            ship.getFinalShip()[4] = ship.getPowerCores()[index];
            powerCore = 1;
        }
        if(powerCore+extraPart+engine+mainBody+cannon == 5){
            activity.setStartGameButton(true);
        }
    }

    @Override
    public void onStartGamePressed() {
        activity.startGame();
    }

    @Override
    public void onResume() {

    }

    @Override
    public IView getView() {
        return null;
    }

    @Override
    public void setView(IView view) {

    }
}
