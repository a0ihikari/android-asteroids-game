package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.StaticObj;

/**
 * Data Access Object for the Level_Background_Obj table.
 */
public class LevelBackgroundObj_DAO extends Game_DAO {
    public static final LevelBackgroundObj_DAO SINGLETON = new LevelBackgroundObj_DAO();

    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr, String number) throws JSONException {
        ContentValues values = new ContentValues();
        boolean result = false;
        String[] position;
        for (int i = 0; i < objArr.length(); ++i) {
            position = objArr.getJSONObject(i).getString("position").split(",");
            values.put("position_X", position[0]);
            values.put("position_Y", position[1]);
            values.put("objectId", objArr.getJSONObject(i).getString("objectId"));
            values.put("scale", objArr.getJSONObject(i).getString("scale"));
            values.put("level_number", number);
            long id = getDb().insert("Level_Background_Obj", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }
}
