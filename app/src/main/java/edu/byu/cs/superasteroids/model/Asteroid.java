package edu.byu.cs.superasteroids.model;

/**
 * It is a super class for all the asteroid objects on the game.
 * @see MovingObj
 */
public class Asteroid extends MovingObj {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
