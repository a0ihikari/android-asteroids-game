package edu.byu.cs.superasteroids.model;

/**
 * It is a class that creates an object that will represent the regular
 * asteroid type on the game.
 * @see Asteroid
 */
public class RegularAst extends Asteroid {
}
