package edu.byu.cs.superasteroids.model;

/**
 * It is a super class for all the objects that will be positioned
 * in space on the game.
 * @see VisualObj
 */
public class ObjInSpace extends VisualObj {
    private int number = 1;
    private int positionX = 0;
    private int positionY = 0;
    private double scale = 1;

    public void setNumber(int number) {
        this.number = number;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public int getNumber() {
        return number;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public double getScale() {
        return scale;
    }
}
