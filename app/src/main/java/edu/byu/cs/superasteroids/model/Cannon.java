package edu.byu.cs.superasteroids.model;

/**
 * It is a class for the cannon objects on the game.
 * @see Attachment
 */
public class Cannon extends Attachment {
    private int emitPoint_X;
    private int emitPoint_Y;
    private String attackImage;
    private int attackImageWidth;
    private int attackImageHeight;
    private String attackSound;
    private int damage;

    public int getEmitPoint_X() {
        return emitPoint_X;
    }

    public void setEmitPoint_X(int emitPoint_X) {
        this.emitPoint_X = emitPoint_X;
    }

    public int getEmitPoint_Y() {
        return emitPoint_Y;
    }

    public void setEmitPoint_Y(int emitPoint_Y) {
        this.emitPoint_Y = emitPoint_Y;
    }

    public String getAttackImage() {
        return attackImage;
    }

    public void setAttackImage(String attackImage) {
        this.attackImage = attackImage;
    }

    public int getAttackImageWidth() {
        return attackImageWidth;
    }

    public void setAttackImageWidth(int attackImageWidth) {
        this.attackImageWidth = attackImageWidth;
    }

    public int getAttackImageHeight() {
        return attackImageHeight;
    }

    public void setAttackImageHeight(int attackImageHeight) {
        this.attackImageHeight = attackImageHeight;
    }

    public String getAttackSound() {
        return attackSound;
    }

    public void setAttackSound(String attackSound) {
        this.attackSound = attackSound;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
