package edu.byu.cs.superasteroids.database;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import edu.byu.cs.superasteroids.model.StaticObj;

/**
 * Data Access Object for the Background_Obj table.
 */
public class BackgroundObj_DAO extends Game_DAO {
    public static final BackgroundObj_DAO SINGLETON = new BackgroundObj_DAO();
    /**
     *Adds the json data to the database.
     */
    public boolean add(JSONArray objArr) throws JSONException {
        ContentValues values = new ContentValues();
        String path;
        boolean result = false;
        for (int i = 0; i < objArr.length(); ++i) {
            path = objArr.getString(i);
            values.put("objects", path);
            long id = getDb().insert("Background_Obj", null, values);

            if (id >= 0) {
                result = true;
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     * Creates a background object based on the parameter received
     * and the query made to the database.
     */
    public StaticObj create(int objId)  {
        StaticObj staObj = new StaticObj();
        Cursor cursor = getDb().rawQuery(
                "SELECT * FROM Background_Obj WHERE PID = " + objId,
                EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                staObj.setImage(cursor.getString(1));

                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return staObj;
    }
}
