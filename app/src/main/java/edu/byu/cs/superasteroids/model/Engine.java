package edu.byu.cs.superasteroids.model;

/**
 * It is a class for the Extra Parts objects on the game.
 * @see Attachment
 */
public class Engine extends Attachment{
    private int baseSpeed;
    private int baseTurnRate;

    public int getBaseSpeed() {
        return baseSpeed;
    }

    public void setBaseSpeed(int baseSpeed) {
        this.baseSpeed = baseSpeed;
    }

    public int getBaseTurnRate() {
        return baseTurnRate;
    }

    public void setBaseTurnRate(int baseTurnRate) {
        this.baseTurnRate = baseTurnRate;
    }
}
