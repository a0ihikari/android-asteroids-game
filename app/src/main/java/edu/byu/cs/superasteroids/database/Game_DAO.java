package edu.byu.cs.superasteroids.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Super class for all Data Access Objects.
 */
public class Game_DAO {
    private SQLiteDatabase db;
    private String table;
    public static final String[] EMPTY_ARRAY_OF_STRINGS = {};
    public static final Game_DAO SINGLETON = new Game_DAO();

    /**
     *Clear the database's tables and resets the PID.
     */
    public void clearDB(String _table){
        setTable(_table);
        int rows;
        rows = db.delete(table, "1", EMPTY_ARRAY_OF_STRINGS);
        if (rows > 0){
            rows++;
        }
        db.execSQL("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME = '" + table + "'");
    }

    public void setTable(String table) {
        this.table = table;
    }

    public void setDb(SQLiteDatabase db) {
        this.db = db;
    }

    public SQLiteDatabase getDb() {
        return db;
    }
}
