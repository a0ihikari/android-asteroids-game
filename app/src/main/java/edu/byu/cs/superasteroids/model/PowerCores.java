package edu.byu.cs.superasteroids.model;

/**
 * It is a class for the power core objects on the game.
 * @see MovingObj
 */
public class PowerCores extends MovingObj {
    private int cannonBoost;
    private int engineBoost;

    public int getCannonBoost() {
        return cannonBoost;
    }

    public void setCannonBoost(int cannonBoost) {
        this.cannonBoost = cannonBoost;
    }

    public int getEngineBoost() {
        return engineBoost;
    }

    public void setEngineBoost(int engineBoost) {
        this.engineBoost = engineBoost;
    }
}
